%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% DiNi(2022) %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%% Function that Update Average Heights and Distances %%%%%%%%%
%%%%%%%%%%%%%%%%%% M.Spitoni - Created on 09/02/2023 %%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%input: UITableData as app.UITable.Data, appData as app.Data  
%output: appData as app.Data  

function appData = f_UpdateAveragesHD(UITableData,appData)
            appData{:,3:11} = UITableData{:,3:11};
            vars = ["Read_1","Read_2"]; % Average
            avg_H = -round(((mean(UITableData{:,vars},2))/100000),5);
            appData{:,12} = avg_H;
            vars = ["Distance1","Distance2"];
            avg_D = round(mean(UITableData{:,vars},2),3);
            appData{:,13} = avg_D;
end
